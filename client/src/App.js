import logo from "./logo.svg";
import "./App.css";
import Message from "./Chat/MessageList/Message/Message";
import MessageList from "./Chat/MessageList/MessageList";
import Chat from "./Chat/Chat";

function App() {
  return <Chat />;
}

export default App;
