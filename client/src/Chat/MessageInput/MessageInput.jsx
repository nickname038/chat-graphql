import React, { useState } from "react";
import { Mutation } from "react-apollo";
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from "../queries";
import "./MessageInput.css";

const MessageInput = (props) => {
  const [messageText, setMessageText] = useState("");

  const onChange = (e) => {
    const value = e.target.value;
    setMessageText(value);
  };

  const _updateStorAfterAddingMessage = (store, newMessage) => {
    const orderBy = "createdAt_DESC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
      },
    });
    data.messages.messageList.unshift(newMessage);
    console.log("data", data);
    store.writeQuery({
      query: MESSAGE_QUERY,
      data
    })
    //setMessageText("");
  };

  return (
    <div className="message-input">
      <form className="message-input-form">
        <textarea
          className="message-input-text"
          placeholder="Message"
          value={messageText}
          onChange={onChange}
        ></textarea>
        <Mutation
          mutation={POST_MESSAGE_MUTATION}
          variables={{ text: messageText }}
          update={(store, { data: { postMessage } }) => {
            console.log(store, { data: { postMessage } });
            _updateStorAfterAddingMessage(store, postMessage);
          }}
        >
          {(postMutation) => (
            <button type="button" className="message-input-button" onClick={(e) => {
              setMessageText("");
              postMutation();
              }}>
              Send
            </button>
          )}
        </Mutation>
      </form>
    </div>
  );
};

export default MessageInput;
