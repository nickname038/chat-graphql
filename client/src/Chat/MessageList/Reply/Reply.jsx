import React from "react";
import "./Reply.css";

const Reply = (props) => {
  return (
    <div className="message-divider-wrapper reply">
      <div
        className={`message`}
      >
        <div className="massage-info-wrapper">
          <div className="message-main">
            <span className="message-text">{props.reply.text}</span>
          </div>
        </div>
      </div>
    </div>
  );
};


export default Reply;