import React from "react";
import "./MessageList.css";
import Message from "./Message/Message";
import { Query } from "react-apollo";
import { MESSAGE_QUERY, NEW_MESSAGE_SUBSCRIPTION } from "../queries";

const MessageList = (props) => {
  const orderBy = "createdAt_DESC";

  const _subscribeToNewMessages = (subscribeToMore) => {
    subscribeToMore({
      document: NEW_MESSAGE_SUBSCRIPTION,
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        const { newMessage } = subscriptionData.data;
        const exist = prev.messages.messageList.find(
          ({ id }) => id === newMessage.id
        );
        if (!exist) return prev;

        console.log("prev", prev);

        return {
          ...prev,
          messages: {
            messageList: [newMessage, ...prev.messages.messageList],
            count: prev.messages.messageList.length + 1,
            __typename: prev.messages.__typename,
          },
        };
      },
    });
  };

  return (
    <Query query={MESSAGE_QUERY} variables={{ orderBy }}>
      {({ loading, error, data, subscribeToMore }) => {
       // _subscribeToNewMessages(subscribeToMore);
        return (
          <div className="message-wrapper">
            <div className="message-list">
              {data?.messages.messageList.map((message) => (
                <Message key={message.id} message={message} />
              ))}
            </div>
          </div>
        );
      }}
    </Query>
  );
};

export default MessageList;
