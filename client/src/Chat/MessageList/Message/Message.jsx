import React, { useState } from "react";
import "./Message.css";
import Reply from "../Reply/Reply";
import ReplyInput from "../ReplyInput/ReplyInput";

const Message = (props) => {
  const [isReplyInputOpen, setIsReplyInputOpen] = useState(false);
  const [isReplyButtonDisabled, setIsReplyButtonDisabled] = useState(false);

  const onClick = (e) => {
    setIsReplyInputOpen(true);
    setIsReplyButtonDisabled(true);
  };

  return (
    <div className="message-divider-wrapper">
      <div className={`message`}>
        <div className="massage-info-wrapper">
          <div className="message-main">
            <span className="message-number">{"#" + props.message.number}</span>
            <span className="message-text">{props.message.text}</span>
          </div>
          <div className="message-footer">
            <button className="message-like">
              <span>{props.message.likeCount}</span>{" "}
              <span className="like-icon">👍</span>
            </button>
            <button className="message-like">
              <span>{props.message.dislikeCount}</span>{" "}
              <span className="like-icon">👎</span>
            </button>
          </div>
        </div>
        <button
          className="reply-button"
          onClick={onClick}
          disabled={isReplyButtonDisabled}
        >
          Reply
        </button>
      </div>
      <div className="replies-wrapper">
        {props.message.replies.map((reply) => {
          return <Reply reply={reply} />;
        })}
        {isReplyInputOpen && (
          <ReplyInput
            setIsReplyInputOpen={setIsReplyInputOpen}
            setIsReplyButtonDisabled={setIsReplyButtonDisabled}
            messageId={props.message.id}
          />
        )}
      </div>
    </div>
  );
};

export default Message;
