import React, { useState } from "react";
import "./ReplyInput.css";
import { Mutation } from "react-apollo";
import { POST_REPLY_MUTATION, MESSAGE_QUERY } from "./../../queries";

const ReplyInput = (props) => {
  const [messageText, setMessageText] = useState("");
  const onChange = (e) => {
    const value = e.target.value;
    setMessageText(value);
  };

  console.log(props);
  console.log({ text: messageText, messageId: props.messageId });

  const onClickOk = () => {
    if (messageText) {
      props.setIsReplyInputOpen(false);
      props.setIsReplyButtonDisabled(false);
      setMessageText("");
    }
  };

  const onClickCancel = () => {
    props.setIsReplyInputOpen(false);
    props.setIsReplyButtonDisabled(false);
    setMessageText("");
  };

  const _updateStorAfterAddingReply = (store, newReply, messageId) => {
    const orderBy = "createdAt_DESC";
    const data = store.readQuery({
      query: MESSAGE_QUERY,
      variables: {
        orderBy,
      },
    });

    const repliedMessage = data.messages.messageList.find(
      (item) => item.id === messageId
    );

    console.log(data);

    repliedMessage.replies.push(newReply);
    store.writeQuery({ query: MESSAGE_QUERY, data });
  };

  return (
    <div className="modal-wrapper">
      <textarea
        className="edit-message-input"
        value={messageText}
        onChange={onChange}
      ></textarea>
      <div className="buttons-wrapper">
        <Mutation
          mutation={POST_REPLY_MUTATION}
          variables={{ messageId: props.messageId, text: messageText }}
          update={(store, { data: { postReply } }) => {
            console.log(store, { data: { postReply } });
            _updateStorAfterAddingReply(store, postReply, props.messageId);
          }}
        >
          {(postReply) => (
            <button
              type="button"
              className="edit-message-button"
              onClick={(e) => {
                if (messageText) {
                  postReply();
                  onClickCancel();
                }
              }}
            >
              OK
            </button>
          )}
        </Mutation>
        <button
          type="button"
          className="edit-message-close"
          onClick={onClickCancel}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default ReplyInput;
