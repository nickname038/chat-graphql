import gql from "graphql-tag";

export const MESSAGE_QUERY = gql`
  query messageQuery($orderBy: MessageOrderByInput) {
    messages(orderBy: $orderBy) {
      count
      messageList {
        id
        text
        likeCount
        dislikeCount
        number
        replies {
          id
          text
        }
      }
    }
  }
`;

export const POST_MESSAGE_MUTATION = gql`
  mutation PostMutation($text: String!) {
    postMessage(text: $text) {
      id
      text
      likeCount
      dislikeCount
      number
      replies {
        id
        text
      }
    }
  }
`;

export const POST_REPLY_MUTATION = gql`
  mutation postReply($messageId: ID!, $text: String!) {
    postReply(messageId: $messageId, text: $text ) {
      id
      text
    }
  }
`;

export const UPDATE_MESSAGE_MUTATION = gql`
  mutation PostMutation($data: MessageInput!, $messageId: ID!) {
    updateMessage(data: $data, messageId: $messageId) {
      id
      text
      likeCount
      dislikeCount
      number
      replies {
        id
        text
      }
    }
  }
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
  subscription {
    newMessage {
      id
      text
      likeCount
      dislikeCount
      number
      replies {
        id
        text
      }
    }
  }
`;
