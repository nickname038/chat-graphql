import "./Chat.css";
import MessageInput from "./MessageInput/MessageInput";
import MessageList from "./MessageList/MessageList";

const Chat = (props) => {

  return (
    <div className="chat">
        <div className="chat-components-wrapper">
          <MessageList />
          <MessageInput />
        </div>
    </div>
  );
};

export default Chat;
