let counter = 1;

function updateMessage(parent, args, context, info) {
  return context.prisma.updateMessage({
    data: args.data,
    where: { id: args.messageId },
  });
}

function postMessage(parent, args, context, info) {
  return context.prisma.createMessage({
    likeCount: 0,
    dislikeCount: 0,
    number: counter++,
    text: args.text,
  });
}

async function postReply(parent, args, context, info) {
  console.log(args);
  const messageExist = await context.prisma.$exists.message({
    id: args.messageId,
  });

  if (!messageExist) {
    throw new Error(`Message with ID ${args.messageId} does not exist!`);
  }

  return context.prisma.createReply({
    message: { connect: { id: args.messageId } },
    text: args.text,
  });
}

module.exports = {
  postMessage,
  postReply,
  updateMessage,
};
