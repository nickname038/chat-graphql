const { GraphQLServer } = require("graphql-yoga");
const { prisma } = require("./prisma/generated/prisma-client");
const Query = require("./src/resolvers/Query");
const Mutation = require("./src/resolvers/Mutation");
const Subscription = require("./src/resolvers/Subscription");
const Message = require("./src/resolvers/Message");
const Reply = require("./src/resolvers/Reply");

const resolvers = {
  Query,
  Mutation,
  Subscription,
  Message,
  Reply,
};

const server = new GraphQLServer({
  typeDefs: "src/schema.graphql",
  resolvers,
  context: { prisma },
});

server.start(() => console.log("Server is running on localhost:4000"));
